/**
  ******************************************************************************
  * @file    include/Climate.h
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    12 марта 2018
  * @brief   Заголовок от Climate.cpp
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2018 ООО "РПС"</center></h2>
  ******************************************************************************
  */

#ifndef CLIMATE_H_
#define CLIMATE_H_

#include <FreeRTOS.h>
#include <task.h>
#include <timers.h>
#include <stm32f10x.h>
#include "common.h"
#include "I2CDriver.h"
#include "SHT30.h"
#include "SA56004.h"
#include "slave.pb.h"


#define THRESHOLD_LOW_DEFAULT   (-5.0f + 237.15f)
#define THRESHOLD_HIGH_DEFAULT  (50.0f + 273.15f)
#define THRESHOLD_HUMIDITY      (95.0f)


typedef struct ClimateStruct_ {
	float TemperatureLocal;			/// Температура локального датчика
	float TemperatureExternal;		/// Температура внешнего датчика
	float TemperatureLocalAlt;		/// Температура в датчике влажности
	float Humidity;					/// Влажность относительная

	bool Heater;					/// Состояние печки
	bool Cooler;					/// Состояние вентилятора

	bool isConfig;
	ClimateMode AutomaticMode;				/// Режим работы (автоматический или ручной)
	TemperatureChannel Channel;
	float ThresholdTLow;
	float ThresholdTHigh;
	float ThresholdHumidity;
} ClimateStruct;


#define EEP_VIRTADDRESS_TLOW     (0)
#define EEP_VIRTADDRESS_THIGH    (EEP_VIRTADDRESS_TLOW + 2)
#define EEP_VIRTADDRESS_HUMIDITY (EEP_VIRTADDRESS_THIGH + 2)
#define EEP_VIRTADDRESS_MODE     (EEP_VIRTADDRESS_MODE + 1)
#define EEP_VIRTADDRESS_CHANNEL  (EEP_VIRTADDRESS_MODE + 1)


class Climate : public TaskBase {
public:
	static Climate &Instance() {
		static Climate s;
		return s;
	}
	void task();
	static void task_climate(void *param) {
		static_cast<Climate *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	QueueHandle_t xQueueData;
	void PrintClimateData(const ClimateStruct &c);

private:
	I2CDriver *m_pI2CDriver = NULL;
	SHT30 *m_pSHT30 = NULL;
	SA56004 *m_pSA56 = NULL;

	float GetEepTLow();
	float GetEepTHigh();
	float GetEepHumidity();
	ClimateMode GetEepMode();
	TemperatureChannel GetEepChannel();

	void StoreFloat(float data, uint16_t offset);
	void StoreUint16(uint16_t data, uint16_t offset);
	void SaveConfig();

private:
	Climate();
	~Climate() {}
	Climate(Climate const &) = delete;
	Climate& operator= (Climate const &) = delete;

	TimerHandle_t xTimer60s;
	SemaphoreHandle_t xSemaphoreTimer60s;

	TimerHandle_t xTimer1s;
	SemaphoreHandle_t xSemaphoreTimer1s;

	QueueSetHandle_t xQueueSet;
	ClimateStruct m_xClimateData;

	ClimateMode m_bAutomaticMode = ClimateMode_MANUAL;
	void ProcessAutomaticRegulation();
};


#endif /* CLIMATE_H_ */
