/**
  ******************************************************************************
  * @file    src/Climate.cpp
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    12 марта 2018
  * @brief   Реализация класса Climate
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2018 ООО "РПС"</center></h2>
  ******************************************************************************
  */


#include <cstdlib>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <queue.h>
//#include <SEGGER_RTT.h>
#include "hardware.h"
#include "Climate.h"
#include "eeprom.h"
//#include "profile.h"

uint16_t VirtAddVarTab[NumbOfVar] = {0, 2, 4, 6, 8, 10, 11, 12};


static void TimerCallback(TimerHandle_t xTimer) {
SemaphoreHandle_t sem = static_cast<SemaphoreHandle_t>(pvTimerGetTimerID(xTimer));
	xSemaphoreGive(sem);
}


Climate::Climate() {
	m_xClimateData.TemperatureLocal = 24.0f + 273.0f;
	m_xClimateData.TemperatureLocalAlt = 24.0f + 273.0f;
	m_xClimateData.TemperatureExternal = 24.0f + 273.0f;
	m_xClimateData.Humidity = 85.0f;
	m_xClimateData.Cooler = false;
	m_xClimateData.Heater = false;
	m_xClimateData.AutomaticMode = ClimateMode_MANUAL;
	m_xClimateData.ThresholdTHigh = THRESHOLD_HIGH_DEFAULT;
	m_xClimateData.ThresholdTLow = THRESHOLD_LOW_DEFAULT;

	m_pI2CDriver = new I2CDriver(I2C2);
	assert_param(m_pI2CDriver);

	m_pSHT30 = new SHT30(m_pI2CDriver, 0x88);
	assert_param(m_pSHT30);

	m_pSA56 = new SA56004(m_pI2CDriver, 0x98);
	assert_param(m_pSA56);

	xSemaphoreTimer1s = xSemaphoreCreateBinary();
	assert_param(xSemaphoreTimer1s);
#if (configUSE_TRACE_FACILITY == 1)
	vTraceSetSemaphoreName(xSemaphoreTimer1s, "Climate_1s");
#endif
	xSemaphoreTake(xSemaphoreTimer1s, 0);

	xSemaphoreTimer60s = xSemaphoreCreateBinary();
	assert_param(xSemaphoreTimer60s);
#if (configUSE_TRACE_FACILITY == 1)
	vTraceSetSemaphoreName(xSemaphoreTimer60s, "Climate_60s");
#endif
	xSemaphoreTake(xSemaphoreTimer60s, 0);

	xQueueSet = xQueueCreateSet(1 + 1);
	assert_param(xQueueSet);
#if (configUSE_TRACE_FACILITY == 1)
	vTraceSetQueueName(xQueueSet, "ClimateQueueSet");
#endif
	xQueueAddToSet(xSemaphoreTimer1s, xQueueSet);
	xQueueAddToSet(xSemaphoreTimer60s, xQueueSet);


	xTimer1s = xTimerCreate("Timer1s", 1 * 1000, pdTRUE, xSemaphoreTimer1s, TimerCallback);
	assert_param(xTimer1s);

	xTimer60s = xTimerCreate("Timer60s", 60 * 1000, pdTRUE, xSemaphoreTimer60s, TimerCallback);
	assert_param(xTimer60s);

	xQueueData = xQueueCreate(1, sizeof(ClimateStruct));
	assert_param(xQueueData);
#if (configUSE_TRACE_FACILITY == 1)
	vTraceSetQueueName(xQueueData, "ClimateData");
#endif
	xQueueOverwrite(xQueueData, &m_xClimateData);

	xTaskCreate(task_climate, "Climate", configTASK_CLIMATE_STACK, this, configTASK_CLIMATE_PRIORITY, &handle);
	assert_param(handle);
}


void Climate::task() {

	m_pSA56->Init();

	xTimerStart(xTimer1s, 1);
	xTimerStart(xTimer60s, 1);

	FLASH_Unlock();
	EE_Init();

	m_xClimateData.ThresholdTLow = GetEepTLow();
	m_xClimateData.ThresholdTHigh = GetEepTHigh();
	m_xClimateData.ThresholdHumidity = GetEepHumidity();
	m_xClimateData.AutomaticMode = m_bAutomaticMode = GetEepMode();
	m_xClimateData.Channel = GetEepChannel();
	m_bAutomaticMode = m_xClimateData.AutomaticMode;
	xQueueOverwrite(xQueueData, &m_xClimateData);

ClimateStruct lc;

	for (;;) {
		QueueSetMemberHandle_t event = xQueueSelectFromSet(xQueueSet, 1000);
		if (event == xSemaphoreTimer1s) {
			xSemaphoreTake(event, 0);
			xQueuePeek(xQueueData, &lc, 0);

			if (lc.AutomaticMode != m_bAutomaticMode) {
				m_bAutomaticMode = lc.AutomaticMode;
				_log("Automatic Mode changed to %s\n", m_bAutomaticMode == ClimateMode_AUTO ? "True" : "False");
				if (m_bAutomaticMode == true) {
					CLIMATE_HEATER_EN = 0;
					CLIMATE_COOLER_EN = 0;
				}
			}

			if (lc.isConfig == true) {
			    _log("Configure climate\n");
			    m_xClimateData.ThresholdHumidity = lc.ThresholdHumidity;
			    m_xClimateData.ThresholdTHigh = lc.ThresholdTHigh;
			    m_xClimateData.ThresholdTLow = lc.ThresholdTLow;
			    m_xClimateData.Channel = lc.Channel;
			    m_xClimateData.isConfig = false;
			    m_xClimateData.AutomaticMode = m_bAutomaticMode;
			    SaveConfig();
			}

			if (m_bAutomaticMode == ClimateMode_MANUAL) {
				CLIMATE_COOLER_EN = lc.Cooler ? 1 : 0;
				CLIMATE_HEATER_EN = lc.Heater ? 1 : 0;
			}

			if (m_pSA56->UpdateData() == false) {
				_log("SA56 Err\n");
				_log("SA56 reset hardware: %s\n", m_pI2CDriver->ResetHardware() ? "OK" : "Err");
			} else {
			    m_xClimateData.TemperatureLocal = m_pSA56->GetTemperature(SA56004::Sensor_Internal);
			    m_xClimateData.TemperatureExternal = m_pSA56->GetTemperature(SA56004::Sensor_Remote);
			}

			vTaskDelay(5);
			if (m_pSHT30->UpdateData() == false) {
				_log("SHT Err\n");
				_log("SHT Reset hardware: %s\n", m_pI2CDriver->ResetHardware() ? "OK" : "Err");
			} else {
			    m_xClimateData.TemperatureLocalAlt = m_pSHT30->GetTemperature();
                m_xClimateData.Humidity = m_pSHT30->GetHumidityRel();
			}

			m_xClimateData.Cooler = (CLIMATE_COOLER_EN == 1);
			m_xClimateData.Heater = (CLIMATE_HEATER_EN == 1);

			m_xClimateData.AutomaticMode = m_bAutomaticMode;
			xQueueOverwrite(xQueueData, &m_xClimateData);

		} else if (event == xSemaphoreTimer60s) {
			xSemaphoreTake(event, 0);

			if (m_bAutomaticMode == ClimateMode_AUTO)
				ProcessAutomaticRegulation();

			m_xClimateData.Cooler = (CLIMATE_COOLER_EN == 1);
			m_xClimateData.Heater = (CLIMATE_HEATER_EN == 1);
			m_xClimateData.AutomaticMode = m_bAutomaticMode;

			xQueueOverwrite(xQueueData, &m_xClimateData);

		} else {
			__NOP();
		}
	}
}

float Climate::GetEepTLow() {
uint16_t data[2];
uint16_t ret0, ret1;
float retf;

    ret0 = EE_ReadVariable(VirtAddVarTab[0], &data[0]);
    ret1 = EE_ReadVariable(VirtAddVarTab[1], &data[1]);

    if ((ret0 != 0) || (ret1 != 0)) {
        _log("Variable TLow not found\n");
        retf = THRESHOLD_LOW_DEFAULT;
        memcpy(data, &retf, sizeof(retf));
        ret0 = EE_WriteVariable(VirtAddVarTab[0], data[0]);
        if (ret0 != FLASH_COMPLETE) {
            _log("Write error\n");
        }
        ret0 = EE_WriteVariable(VirtAddVarTab[1], data[1]);
        if (ret0 != FLASH_COMPLETE) {
            _log("Write 2 error\n");
        }
    } else {
        _log("Read successful\n");
        memcpy(&retf, data, sizeof(data));
    }
    return retf;
}


float Climate::GetEepTHigh() {
uint16_t data[2];
uint16_t ret0, ret1;
float retf;

    ret0 = EE_ReadVariable(VirtAddVarTab[2], &data[0]);
    ret1 = EE_ReadVariable(VirtAddVarTab[3], &data[1]);

    if ((ret0 != 0) || (ret1 != 0)) {
        _log("Variable THigh not found\n");
        retf = THRESHOLD_HIGH_DEFAULT;
        memcpy(data, &retf, sizeof(retf));
        ret0 = EE_WriteVariable(VirtAddVarTab[2], data[0]);
        if (ret0 != FLASH_COMPLETE) {
            _log("Write error\n");
        }
        ret0 = EE_WriteVariable(VirtAddVarTab[3], data[1]);
        if (ret0 != FLASH_COMPLETE) {
            _log("Write 2 error\n");
        }
    } else {
        _log("Read successful\n");
        memcpy(&retf, data, sizeof(data));
    }
    return retf;
}

float Climate::GetEepHumidity() {
uint16_t data[2];
uint16_t ret0, ret1;
float retf;

    ret0 = EE_ReadVariable(VirtAddVarTab[4], &data[0]);
    ret1 = EE_ReadVariable(VirtAddVarTab[5], &data[1]);

    if ((ret0 != 0) || (ret1 != 0)) {
        _log("Variable Humidity not found\n");
        retf = THRESHOLD_HUMIDITY;
        memcpy(data, &retf, sizeof(retf));
        ret0 = EE_WriteVariable(VirtAddVarTab[4], data[0]);
        if (ret0 != FLASH_COMPLETE) {
            _log("Write error\n");
        }
        ret0 = EE_WriteVariable(VirtAddVarTab[5], data[1]);
        if (ret0 != FLASH_COMPLETE) {
            _log("Write 2 error\n");
        }
    } else {
        _log("Read successful\n");
        memcpy(&retf, data, sizeof(data));
    }
    return retf;
}

ClimateMode Climate::GetEepMode() {
uint16_t data;
uint16_t ret;
ClimateMode retc;

    ret = EE_ReadVariable(VirtAddVarTab[6], &data);
    if (ret != 0) {
        _log("Variable Mode not found\n");
        retc = ClimateMode_MANUAL;
        data = retc;
        ret = EE_WriteVariable(VirtAddVarTab[6], data);
        if (ret != FLASH_COMPLETE) {
            _log("Write error\n");
        }
    } else {
        _log("Read successful\n");
        retc = (ClimateMode)data;
    }
    return retc;
}

TemperatureChannel Climate::GetEepChannel() {
uint16_t data;
uint16_t ret;
TemperatureChannel channel = TemperatureChannel_TC_LOCAL;

    ret = EE_ReadVariable(VirtAddVarTab[7], &data);
    if (ret != 0) {
        _log("Variable Channel not found\n");
        data = TemperatureChannel_TC_LOCAL;
        ret = EE_WriteVariable(VirtAddVarTab[7], data);
        if (ret != FLASH_COMPLETE) {
            _log("Write error\n");
        }
    } else {
        _log ("Read succescfull\n");
        channel = (TemperatureChannel)data;
    }
    return channel;
}


void Climate::SaveConfig() {
    StoreFloat(m_xClimateData.ThresholdTLow, 0);
    StoreFloat(m_xClimateData.ThresholdTHigh, 2);
    StoreFloat(m_xClimateData.ThresholdHumidity, 4);
    StoreUint16(m_xClimateData.AutomaticMode, 6);
    StoreUint16(m_xClimateData.Channel, 7);
}

void Climate::StoreFloat(float data, uint16_t offset) {
uint16_t buf[2];
    memcpy(buf, &data, sizeof(data));
    if (EE_WriteVariable(VirtAddVarTab[offset], buf[0]) != FLASH_COMPLETE) {
        _log("Write error\n");
    }
    if (EE_WriteVariable(VirtAddVarTab[offset+1], buf[1]) != FLASH_COMPLETE) {
        _log("Write error\n");
    }
}

void Climate::StoreUint16(uint16_t data, uint16_t offset) {
    if (EE_WriteVariable(VirtAddVarTab[offset], data) != FLASH_COMPLETE) {
        _log("Write error\n");
    }
}

void Climate::ProcessAutomaticRegulation() {
	float temperature = (m_xClimateData.Channel == TemperatureChannel_TC_LOCAL) ? m_xClimateData.TemperatureLocal : m_xClimateData.TemperatureExternal;
	float humidity = m_xClimateData.Humidity;

	float t_low = m_xClimateData.ThresholdTLow;
	float t_high = m_xClimateData.ThresholdTHigh;
	float t_hum = m_xClimateData.ThresholdHumidity;

	int heater, cooler;

	if (temperature > t_high) {
	    // Перегрев
	    heater = 0;
	    cooler = 1;
	} else {
	    if (temperature < t_low) {
	        // Слишком сильно остыли
	        heater = 1;
	        cooler = 0;
	    } else {
	        // Температура в норме
	        heater = 0;
	        cooler = 0;
	    }
	}

	int last_heater = heater;
	if ((humidity > t_hum) && (temperature < t_high)) {
	    heater = 1;
	} else {
	    heater = last_heater;
	}

	CLIMATE_COOLER_EN = cooler;
	CLIMATE_HEATER_EN = heater;

	_log("Automatic: Heater(%d), Cooler(%d)\n", CLIMATE_HEATER_EN, CLIMATE_COOLER_EN);
}


void Climate::PrintClimateData(const ClimateStruct &c) {
char str[16];
	FloatToString(str, c.TemperatureLocal - 273.15f, 3, 3);
	_log("Temperature Local: %s C\n", str);

	FloatToString(str, c.TemperatureExternal - 273.15f, 3, 3);
	_log("Temperature Remote: %s C\n", str);

	FloatToString(str, c.TemperatureLocalAlt - 273.15f, 3, 3);
	_log("Temperature SHT30: %s C\n", str);

	FloatToString(str, c.Humidity, 3, 3);
	_log("Humidity: %s%%\n", str);

	_log("Heater: %s\n", c.Heater ? "ON" : "OFF");
	_log("Cooler: %s\n\n", c.Cooler ? "ON" : "OFF");
}


