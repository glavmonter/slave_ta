from google.protobuf.message import DecodeError
import slave_pb2


datas = [b'\xC0\x81\x50\x03\x30\x80\x04\xEA',
         b'\xC0\x81\x50\x03\x30\xDB\xDC\x04\x71',
         b'\xC0\x81\x50\x03\x30\xC0\x04\x71',
         b'\xC0\x81\x50\x03\x30\x80\x04\xEA',
         b'\xC0\x81\x50\x03\x30\xDB\xDC\x04\x71',
         b'\xC0\x81\x50\x03\x30\xC0\x04\x71']

for d in datas:
    responce = slave_pb2.ResponseAll()
    ld = d[4:-1]
    try:
        responce.ParseFromString(ld)
    except DecodeError as e:
        print(f'Decode error: {e}')
    print(f'{d}: {responce}')
