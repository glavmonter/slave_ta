import struct
from PyQt5.QtCore import QCoreApplication, pyqtSlot, QSettings
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo

from SlaveGui_ui import Ui_MainWindow
from WakeReader import WakeReader, WakeStatus
from SlaveTA import Commands
import slave_pb2


def _relays_to_humane(data):
    if data is not None:
        return '{:05b}_{:05b}'.format((data & 0b1111100000) >> 5, data & 0b11111)
    else:
        return 'None'


def _port_to_humane(data):
    if data is not None:
        return '{:04b}'.format(data)
    else:
        return 'None'


def to_hex(b):
    return ' '.join('{:02X}'.format(x) for x in b)


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)

        self.populateSerialPorts()

        self.cmbBaudrates.currentIndexChanged[int].connect(self.baudrateChanged)
        self.btnRefresh.clicked.connect(self.populateSerialPorts)
        self.spinBaudrate.valueChanged[int].connect(self.spinBaudrateChanged)

        self.btnConnect.clicked.connect(self.connectClicked)
        self.btnDisconnect.clicked.connect(self.disconnectClicked)
        self.labelComStatus.setText('COM disconnected')

        self.serialPort = QSerialPort()
        self.serialPort.errorOccurred.connect(self.serialPortError)
        self.serialPort.readyRead.connect(self.serialPortReadyRead)
        self.wakeReader = WakeReader()

        self.btnInfo.clicked.connect(self.getInfo)
        self.process_readall(b'');

    @pyqtSlot()
    def populateSerialPorts(self):
        settings = QSettings()
        lastBaudrate = settings.value('baudrate', 115200)

        self.cmbComPorts.clear()
        self.cmbBaudrates.clear()

        for l in QSerialPortInfo.availablePorts():
            port = '{name:}: {desc:}'.format(name=l.portName(), desc=l.description())
            self.cmbComPorts.addItem(port, userData=l.portName())

        for baud in QSerialPortInfo.standardBaudRates():
            self.cmbBaudrates.addItem('{} bps'.format(baud), userData=baud)

        self.cmbBaudrates.addItem('Custom baudrate', userData=-1)

        custom = int(settings.value('custombaud', 115200))
        self.spinBaudrate.setValue(custom)
        baudrate_index = self.cmbBaudrates.findData(lastBaudrate)
        if baudrate_index != -1:
            self.cmbBaudrates.setCurrentIndex(baudrate_index)
        else:
            self.cmbBaudrates.setCurrentIndex(self.cmbBaudrates.count() - 1)
            self.spinBaudrate.setValue(custom)
            self.spinBaudrate.setEnabled(True)

    def baudrateChanged(self, index):
        baudrate = self.cmbBaudrates.itemData(index)
        print('index: {}, data: {}'.format(index, baudrate))
        if baudrate == -1:
            baudrate = self.spinBaudrate.value()

        self.spinBaudrate.setEnabled(self.cmbBaudrates.itemData(index) == -1)
        settings = QSettings()
        settings.setValue('baudrate', baudrate)

    def spinBaudrateChanged(self, value):
        settings = QSettings()
        settings.setValue('baudrate', value)
        settings.setValue('custombaud', value)

    def openPort(self, portName, baudrate):
        self.serialPort.setPortName(portName)
        self.serialPort.setBaudRate(baudrate)
        self.serialPort.setDataBits(QSerialPort.Data8)
        self.serialPort.setFlowControl(QSerialPort.NoFlowControl)
        self.serialPort.setParity(QSerialPort.NoParity)
        self.serialPort.setStopBits(QSerialPort.OneStop)
        return self.serialPort.open(QSerialPort.ReadWrite)

    @pyqtSlot()
    def connectClicked(self):
        print('Open COM port')
        portName = self.cmbComPorts.itemData(self.cmbComPorts.currentIndex())
        baudrate = self.cmbBaudrates.itemData(self.cmbBaudrates.currentIndex())
        if baudrate == -1:
            baudrate = self.spinBaudrate.value()

        if self.openPort(portName, baudrate):
            self.cmbComPorts.setEnabled(False)
            self.btnRefresh.setEnabled(False)
            self.spinBaudrate.setEnabled(False)
            self.cmbBaudrates.setEnabled(False)
            self.btnDisconnect.setEnabled(True)
            self.btnConnect.setEnabled(False)
            self.labelComStatus.setText('{name:} connected on speed {baud:} bps'.format(name=portName, baud=baudrate))
        else:
            self.disconnectClicked()

    @pyqtSlot()
    def disconnectClicked(self):
        print('Disconnect and close COM port')
        self.serialPort.close()
        self.cmbComPorts.setEnabled(True)
        self.btnRefresh.setEnabled(True)
        self.cmbBaudrates.setEnabled(True)
        self.spinBaudrate.setEnabled(self.cmbBaudrates.itemData(self.cmbBaudrates.currentIndex()) == -1)
        self.btnConnect.setEnabled(True)
        self.btnDisconnect.setEnabled(False)
        self.labelComStatus.setText('COM disconnected')

    def serialPortError(self, error):
        if error != QSerialPort.NoError and error != QSerialPort.NotOpenError:
            print('Error: {}'.format(error))
            self.disconnectClicked()


    def serialPortReadyRead(self):
        data = self.serialPort.readAll()
        print('Received: {} bytes'.format(data.size()))
        for d in data.data():
            ret = self.wakeReader.process_in_byte(d)
            if ret == WakeStatus.STA_READY:
                print('Wake readed {} bytes'.format(len(self.wakeReader.get_wake_data())))
                self.process_input_wake(self.wakeReader.get_wake_data(), self.wakeReader.get_command())
            elif ret == WakeStatus.STA_CRC_ERROR:
                print('CRC error')
            elif ret == WakeStatus.STA_DATA_TO_MUCH:
                print('Дохера даныых')

    def process_input_wake(self, wake_data, command):
        print('process_input_wake: {}'.format(to_hex(wake_data)))
        if command == Commands.INFO.value:
            self.process_info(wake_data)
        elif command == Commands.READ_ALL.value:
            self.process_readall(wake_data)

    def process_info(self, data):
        try:
            flash_size = struct.unpack('<H', data[0:2])[0]
            unique_id = data[2:14]
            version = struct.unpack('<I', data[14:18])[0]
            name = data[18:].decode('utf-8')
        except IndexError as e:
            print('Decode Info error')
            return
        self.lineEditInfoID.setText('ID: ' + ''.join('{:02X}'.format(x) for x in unique_id))
        self.lineEditInfoName.setText('Name: {name:} ({fs:} kB), ver: {ver:}'.format(name=name, fs=flash_size, ver=version))

    def process_readall(self, data):
        responce_all = slave_pb2.ResponseAll()
        responce_all.ParseFromString(data)

        porta_idr = responce_all.PORTA_IDR
        portb_idr = responce_all.PORTB_IDR
        porta_odr = responce_all.PORTA_ODR
        portb_odr = responce_all.PORTB_ODR
        relays_idr = responce_all.RELAYS_IDR
        relays_odr = responce_all.RELAYS_ODR
        wieg1_size = 0
        wieg1_data = b''
        wieg2_size = 0
        wieg2_data = b''

        wieg = responce_all.WiegandCh1
        if wieg is not None:
            wieg1_size = wieg.size
            wieg1_data = wieg.data

        wieg = responce_all.WiegandCh2
        if wieg is not None:
            wieg2_size = wieg.size
            wieg2_data = wieg.data

        self.lineEditPortIDR.setText('Port Input : [A: {}] [B: {}]'.format(_port_to_humane(porta_idr),
                                                                           _port_to_humane(portb_idr)))
        self.lineEditPortODR.setText('Port Output: [A: {}] [B: {}]'.format(_port_to_humane(porta_odr),
                                                                           _port_to_humane(portb_odr)))
        self.lineEditRelayIDR.setText('Relays Input : [{}]'.format(_relays_to_humane(relays_idr)))
        self.lineEditRelayODR.setText('Relays Output: [{}]'.format(_relays_to_humane(relays_odr)))
        self.lineEditWiegand1.setText('Wiegand 1: {}, [{}]'.format(wieg1_size, to_hex(wieg1_data)))
        self.lineEditWiegand2.setText('Wiegand 2: {}, [{}]'.format(wieg2_size, to_hex(wieg2_data)))

    def getInfo(self):
        print('Info')
        request = b'\xC0\x82\x03\x00\x23'
        self.serialPort.write(request)


if __name__ == '__main__':
    import sys
    QCoreApplication.setOrganizationName('RPS')
    QCoreApplication.setApplicationName('SlaveGui')
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
