try:
    import crcmod
except ModuleNotFoundError as err:
    print('Import module crcmod error: {}'.format(str(err)))
    print('Install module crcmod')
    exit(-1)

from enum import Enum
import struct

FEND = b'\xC0'
FESC = b'\xDB'
TFEND = b'\xDC'
TFESC = b'\xDD'

'''
def wake_encode(cmd, data, adr=None):
    d_for_crc = FEND
    d_for_tx = b''

    if adr is not None:
        d_for_crc += struct.pack('<B', adr & 0x7F)
        d_for_tx += struct.pack('<B', adr + 0x80)

    d_for_crc += struct.pack('<B', cmd)
    d_for_tx += struct.pack('<B', cmd)

    d_for_crc += struct.pack('<H', len(data)) + data
    d_for_tx += struct.pack('<H', len(data)) + data

    crc16_func = crcmod.predefined.mkPredefinedCrcFun('xmodem')
    crc = crc16_func(d_for_crc)
    print('CRC16: {}'.format(struct.pack('<H', crc).hex().upper()))

    d_for_tx += struct.pack('<H', crc)
    d_for_tx = d_for_tx.replace(FESC, FESC + TFESC)
    d_for_tx = d_for_tx.replace(FEND, FESC + TFEND)

    return FEND + d_for_tx


def wake_decode(data):
    return data.replace(FESC + TFEND, FEND).replace(FESC + TFESC, FESC)


def wake_check_crc(data):
    if len(data) < 5:
        return False

    d_crc = b''
    d_crc += data[0:1]
    d_crc += bytes([data[1] & 0x7F])
    d_crc += data[2:-1]

    crc16_func = crcmod.predefined.mkPredefinedCrcFun('xmodem')
    crc_calc = crc16_func(d_crc)

    return crc_calc == data[-1:][0]
'''


class WakeStatus(Enum):
    STA_INIT = 1
    STA_RECEIVING = 2
    STA_READY = 3
    STA_CRC_ERROR = 4
    STA_DATA_TO_MUCH = 5


class RxFsmState(Enum):
    WAIT_FEND = 1
    WAIT_ADDR = 2
    WAIT_CMD = 3
    WAIT_NBT0 = 4
    WAIT_DATA = 6
    WAIT_CRC0 = 7
    WAIT_CRC1 = 8
    WAIT_CARR = 9


class WakeReader:
    def __init__(self, address=1):
        self.address = address
        self.last_byte = b''
        self.rx_fsm = RxFsmState.WAIT_ADDR
        self.nbt0 = 0
        self.nbt1 = 0
        self.crc0 = 0
        self.crc1 = 0
        self.command = 0
        self.wake_data = b''
        self.wake_data_size = 0

    def get_command(self):
        return self.command

    def get_wake_data(self):
        return self.wake_data

    def process_in_byte(self, data_byte):
        status = WakeStatus.STA_INIT

        data_byte = struct.pack('B', data_byte)
        if data_byte == FEND:
            self.wake_data = b''
            self.wake_data_size = 0
            self.last_byte = data_byte
            self.rx_fsm = RxFsmState.WAIT_ADDR
            return WakeStatus.STA_INIT

        if self.rx_fsm == RxFsmState.WAIT_FEND:
            return WakeStatus.STA_INIT

        pre = self.last_byte
        self.last_byte = data_byte
        if pre == FESC:
            if data_byte == TFESC:
                data_byte = FESC
            elif data_byte == TFEND:
                data_byte = FEND
            else:
                self.rx_fsm = RxFsmState.WAIT_FEND
                return WakeStatus.STA_INIT
        else:
            if data_byte == FESC:
                return WakeStatus.STA_INIT

        data_integer = struct.unpack('B', data_byte)[0]
        if self.rx_fsm == RxFsmState.WAIT_ADDR:
            if data_integer & 0x80:
                data_integer = data_integer & 0x7F
                # print('Address: {}'.format(data_integer))
                if not data_integer or (data_integer == self.address):
                    self.rx_fsm = RxFsmState.WAIT_CMD
                    self.address = data_integer
                    return status
                self.rx_fsm = RxFsmState.WAIT_FEND
                return status
            self.rx_fsm = RxFsmState.WAIT_CMD
            return status

        elif self.rx_fsm == RxFsmState.WAIT_CMD:
            if data_integer & 0x80:
                self.rx_fsm = RxFsmState.WAIT_FEND
                return status
            print('Command: {}'.format(data_integer))
            self.command = data_integer
            self.rx_fsm = RxFsmState.WAIT_NBT0
            return status

        elif self.rx_fsm == RxFsmState.WAIT_NBT0:
            self.nbt0 = data_integer
            self.wake_data_size = self.nbt0
            self.rx_fsm = RxFsmState.WAIT_DATA
            # print('Wake Data size: {}'.format(self.wake_data_size))
            return status

        elif self.rx_fsm == RxFsmState.WAIT_DATA:
            if len(self.wake_data) < self.wake_data_size + 1:
                self.wake_data += struct.pack('<B', data_integer)

                if len(self.wake_data) == self.wake_data_size + 1:
                    self.rx_fsm = RxFsmState.WAIT_FEND
                    crc8 = self.wake_data[-1:]
                    # print('CRC8: {}'.format(crc8.hex().upper()))
                    self.wake_data = self.wake_data[:-1]
                    data_to_calculate_crc = FEND + struct.pack('<B', self.address) + \
                                            struct.pack('<B', self.command) + struct.pack('<B', self.wake_data_size) + \
                                            self.wake_data

                    crc8_func = crcmod.predefined.mkPredefinedCrcFun('crc-8-maxim')
                    crc_calc = struct.pack('<B', crc8_func(data_to_calculate_crc))
                    # print('CRC8 calc: {}'.format(crc_calc.hex().upper()))
                    if crc8 == crc_calc:
                        status = WakeStatus.STA_READY
                    else:
                        status = WakeStatus.STA_CRC_ERROR

            else:
                self.rx_fsm = RxFsmState.WAIT_FEND
                status = WakeStatus.STA_DATA_TO_MUCH
                return status
        return status
